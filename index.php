<?php
	$dbhost = 'localhost';
    $dbuser = 'root';
    $dbpass = '';
    $conn = mysqli_connect($dbhost, $dbuser, $dbpass);
        if(! $conn ) {
            die('Could not connect: ' . mysqli_error());
        }
?>	
<html>
	<head>
		<title>www.sebok.com</title>
		
		<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		
		
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
	</head>
	<body>	
	

		<div class="container">
			<!-- header area start-->
			<div class="header">
			
									<header class="header-area">
									<div class="header-top">
										<div class="container">
											<div class="header-top-left">
												<ul class="site-info">
													<li><i class="fa fa-globe"></i> English</li>
													<li><i class="fa fa-mobile"></i> 058383738</li>
													<li><i class="fa fa-paper-plane"></i> info.sebok-team.com</li>
													<li><i class="fa fa-search"></i> Search</li>
												</ul>
											</div>

											<div class="header-top-right">
												<div class="sign-register-option">
													<i class="fa fa-sign-in" aria-hidden="true"></i>
													<a href="#">Sign In</a>
													<span>/</span>
													<a href="#">Register</a>
												</div>
												<div class="social-media-list">
													<ul>
														<li><a href="http://www.facebook.com"><i class="fa fa-facebook"></i></a></li>
														<li><a href="http://www.twitter.com"><i class="fa fa-twitter"></i></a></li>
														<li><a href="http://www.plus.google.com"><i class="fa fa-google-plus"></i></a></li>
														<li><a href="http://www.linkedin.com"><i class="fa fa-linkedin"></i></a></li>
														
													</ul>
												</div>
											</div>
										</div>
									</div>
									
									
									
										<div class="logo">
											<div class="logo-inner">
												<img src="assets/images/sebok.png">
											</div>
										</div>
										
										
										
									<div class="menu">
										<nav id="primary_nav_wrap">
						<ul>
						  <li class="current-menu-item"><a href="#">Home</a></li>
						  <li><a href="#">Service</a>
							<ul>
							  <li><a href="#">On Time</a></li>
							  <li><a href="#">On Budget</a></li>
							  <li><a href="#">On Demand</a></li>
							  <li><a href="#">Brand</a>
								<ul>
								  <li><a href="#">Brand Item</a>
									<ul>
									  <li><a href="#">Import</a></li>
									  <li><a href="#">Export</a></li>
									
									</ul>
								  </li>
								  <li><a href="#">Brand List</a></li>
								</ul>
							  </li>
							  <li><a href="#">Membership</a></li>
							</ul>
						  </li>
						  <li><a href="#">Support</a>
							<ul>
							  <li><a href="#">Online</a></li>
							  <li><a href="#">Offline</a></li>
							  <li><a href="#">Live</a></li>
							</ul>
						  </li>
						  <li><a href="#">Product</a>
							<ul>
							  <li class="dir"><a href="#">New Product</a></li>
							  <li class="dir"><a href="#">Search by Category Way of Selected Product</a>
								<ul>
								  <li><a href="#">Ladys</a></li>
								  <li><a href="#">Gents</a></li>
								  <li><a href="#">Kids</a></li>
								  <li><a href="#">Old</a></li>
								 
								</ul>
							  </li>
							  <li><a href="#">Flat</a></li>
							  <li><a href="#">Car</a></li>
							  <li><a href="#">Electronics</a></li>
							</ul>
						  </li>
						  <li><a href="#"></a></li>
						  <li><a href="#">Portfolio</a></li>
						  <li><a href="#">Career</a></li>
						  <li><a href="#">Contact Us</a></li>
						</ul>
						</nav>
						</div>
										
											
				
			</div>
			<!--header area end-->
			
			
			
			<!--banner area start-->
			<div class="banner">
				<div class="banner-text">
					<h2>Welcome to our</h2>
					<h1>sebok.com for sell and buy product.</h1>
				</div>
			</div>
			<!--banner area end-->
			<div class="product-section section-padding">
				<div class="section-header">
					
				</div>
				<div class="section-body">
					<div class="section-inner">
					
					<table border=6 align=center>
					<h2>Do You Have Something To Sell?</h2>
					<h2>-------------------------------------------</h2>
					<h3>Post your add on sebok.com:</h3>
					<p>Write below your Product Details---</p>
						<form method="post" action="insert.php">
						<label>Product Name:</label><br>
						<input type="text" name="product_name"><br>
						<label>Product Condition:</label><br>
						<input type="radio" name="product_condition" value="Old">Old</br>
						<input type="radio" name="product_condition" value="New">New</br>
						<label>Payment Supported To:</label><br>
						<input type="checkbox" name="payment_way[]" value="Online">Online</br>
						<input type="checkbox" name="payment_way[]" value="Bkash">Bkash</br>
						<input type="checkbox" name="payment_way[]" value="Paypal">Paypal</br>
						<input type="checkbox" name="payment_way[]" value="Visa">Visa</br>
						
						<label>Price:</label><br>
						<input type="text" name="rate"><br>
						<label>Product Brand:</label><br>
						<select name="product_brand">
							<option value="Apple">Apple</option>
							<option value="Samsung">Samsung</option>
							<option value="Walton">Walton</option>
							<option value="LG">LG</option>
							<option value="Intel">Intel</option>
							<option value="Sony">Sony</option>
							<option value="Dell">Dell</option>
							<option value="HP">HP</option>
							<option value="Acer">Acer</option>
							<option value="Lenovo">Lenovo</option>
							<option value="Toshiba">Toshiba</option>
							<option value="Singer">Singer</option>
						</select><br>
						<label>Saller Name:</label><br>
						<input type="text" name="saller_name"><br>
						<label>Saller Mobile:</label><br>
						<input type="text" name="saller_mobile"><br>
						<input type="submit" name="save" value="Save">
					</form>
					</table>
					<?php
					//echo "Entered data successfully!";
					echo "Thanks for use our sebok.com!";
					?>
				
					<a href="<?php echo 'update.php'; ?>">See All Product-</a>	
					
					</div>
				</div>
			
			</div>
			
			<!--team section start-->
			<div class="team-section section-padding">
				<div class="section-header">
					<h2>Our Team Member</h2>
					
					
					<p>Our team is the highly experience in IT.</p>
				</div>
			
				
				<div class="section-body">
					<div class="team-item">
						<div class="team-item-inner">
							<img src="assets/images/team/yela.jpg">
							<h4>Yela</h4>
							<p>Designer</p>
												
							
							
						</div>
					</div>
					
					<div class="team-item">
						<div class="team-item-inner">
							<img src="assets/images/team/nurislam2.jpg">
							<h4>Nur Islam</h4>
							<p>Developer</p>
					
						</div>
					</div>
					
					<div class="team-item">
						<div class="team-item-inner">
							<img src="assets/images/team/rashed.jpg">
							<h4>Rahed</h4>
							<p>CTO</p>
							
						</div>
					</div>
					<div class="team-item">
						<div class="team-item-inner">
							<img src="assets/images/team/nuralam.jpg">
							<h4>Nur Alam</h4>
							<p>SEO Expert</p>
							
						</div>
					</div>
					
					<div class="team-item">
						<div class="team-item-inner">
							<img src="assets/images/team/sabbir.jpg">
							<h4>Sabbir Khan</h4>
							<p>Writer</p>
							
						</div>
					</div>
					
				</div>
			</div>
			<!--team section end-->
			
			
			<!--news section start-->
			<div class="news-section section-padding">
				<div class="section-header">
					<h2>Latest news</h2>
					<p>Buy and sell everything from used cars to mobile phones and computers, or search for property, jobs and more in Bangladesh - for free!.</p>
				</div>
				
				<div class="section-body">
					<div class="news-item">
						<div class="news-item-inner">
							<div class="news-image">
								<img src="assets/images/news/04.jpg">
							</div>
							<div class="news-text">
								<h3>Quickly Delivery</h3>
								<p class="date">29 november, 2017</p>
								<p>Find great deals for used electronics in Bangladesh including mobile phones, computers, laptops, TVs, cameras and much more product for you.</p>
								<a href="">Read More</a>
							</div>
						</div>
					</div>
					
					<div class="news-item">
						<div class="news-item-inner">
							<div class="news-image">
								<img src="assets/images/news/02.jpg">
							</div>
							<div class="news-text">
								<h3>Quickly Support</h3>
								<p class="date">29 november, 2017</p>
								<p>Buy and sell clothing, garments, shoes and other personal items including handbags, perfumes, children's toys and hand made items in Bangladesh.</p>
								<a href="">Read More</a>
							</div>
						</div>
					</div>
					
					<div class="news-item">
						<div class="news-item-inner">
							<div class="news-image">
								<img src="assets/images/news/03.jpg">
							</div>
							<div class="news-text">
								<h3>Advice</h3>
								<p class="date">29 november, 2017</p>
								<p>Classified ads for miscellaneous products and items all over Bangladesh. Buy and sell almost anything. So visit sebok.com for online support. </p>
								<a href="">Read More</a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--news section end-->
			
			
			<!--testimonial section start-->
			<div class="testimonial-section section-padding">
				<div class="section-header">
					<h2>Our Claint Say</h2>
					<p>After buy a Product from <a href="#">sebok.com</a>, I am felling happy.</p>
				</div>
				
				<div class="section-body">
					<div class="section-inner">
					<div class="tesmimonial">
						<p> sebok.com does not specialize in any specific category - here you can buy and sell items in more than 50 different categories. We also carefully review all ads that are being published, to make sure the quality is up to our standards..</p>
						
						<div class="client">
							<img src="assets/images/ceo1.jpg">
						</div>
						<p class="deg">CEO</p>
						
					</div>
					</div>
				</div>
			</div>
			<div class="footer">
			<P>Copyright@sebok2017_Team</P>
			</div>
		</div>
	</body>
</html>